# The DCM Project (Dreamcast Mesh)

.dcm is a new mesh format in development that is optimised loading and rendering models on the SEGA Dreamcast. It is currently used in the following projects:

 - Simulant Engine (https://simulant.dev)
 
## Building

Compiling the code requires meson and ninja:

If you have Ubuntu:
```
    sudo apt-get install meson ninja-build
```

If you have Fedora:
```
    sudo dnf install meson ninja-build
```

To build:
```
    git clone https://gitlab.com/simulant/community/dcm
    cd tools/obj2dcm
    meson setup build/ls
    ninja -C build/
```

After that you'll have obj2dcm in the build folder.

## How do I use it?

OBJ -> DCM:
```
    chmod +x obj2dcm
    ./obj2dcm -i in_file.obj -o out_file.dcm
```
import bpy, array
from bpy_extras.io_utils import ExportHelper
from . import export_mesh_impl

class DCMeshExporter(bpy.types.Operator, ExportHelper):
    
    ## Blender Registration

    bl_idname = "dcmesh.export_mesh"
    bl_label = "Export Mesh"
    bl_options = {'PRESET'}
    filename_ext = '.dcmesh'
    export_text = "DCMesh (.dcmesh)"
 
    @classmethod
    def register(cls):
        bpy.types.TOPBAR_MT_file_export.append(draw_export_menu)

    @classmethod
    def unregister(cls):
        bpy.types.TOPBAR_MT_file_export.remove(draw_export_menu)

    ## Operation
 
    def execute(self, context):
        
        if (self.filepath == ""):
            print("No sutable filename was provided to save to.")
            return {'FINISHED'}
            
        # copy data from the active object
        # (currently limited to one object at a time)

        obj = bpy.context.active_object
        export_mesh_impl.export_mesh_object(obj, self.filepath)
            
        # Parse all the objects in the scene.
        return {'FINISHED'}
    
def draw_export_menu(self, context):
    self.layout.operator(DCMeshExporter.bl_idname, text=DCMeshExporter.export_text)
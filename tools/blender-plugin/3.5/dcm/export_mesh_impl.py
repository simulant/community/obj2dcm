import bpy, array, struct, hashlib

def de_index(verts, idcs):
    return [verts[i] for i in idcs]

def get_texture_name(obj):
    texture_nodes = []
    for mat_slot in obj.material_slots:
        if mat_slot.material:
            if mat_slot.material.node_tree:
                texture_nodes.extend([x for x in mat_slot.material.node_tree.nodes if x.type=='TEX_IMAGE'])

    if len(texture_nodes) > 0:
        texture_name = texture_nodes[0].image.name
    else:
        texture_name = "material"

    return texture_name

def export_mesh_object(obj, filepath):

    if obj.type != 'MESH':
        return

    # Pre-process the data in the scene so it's ready to be written out.
    # This step is necessary because of the differences between Blender and DCMesh:
    # - Blender supports many-sided polygons, DCMesh only supports triangles.
    # - Blender specifies UVs at the polygon-level, DCMesh specifies them per unique vertex.

    blender_verts = obj.data.vertices
    blender_cols = obj.data.vertex_colors
    uv_idx_base = 0
    dcm_verts = []
    dcm_vert_weight_arrays = []
    armature_name = ""

    should_export_weights = obj.vertex_groups != None

    for f in obj.data.polygons:
        
        idcs = f.vertices
        uv_idcs = []
        positions = []
        
        # De-index this Blender polygon and store indices for UVs:
        if len(idcs) == 4:
            positions.extend(de_index(blender_verts, [idcs[0], idcs[2], idcs[1]]))
            positions.extend(de_index(blender_verts, [idcs[0], idcs[3], idcs[2]]))
            uv_idcs.extend([0, 2, 1, 0, 3, 2])
            
        elif len(idcs) == 3:
            positions.extend(de_index(blender_verts, [idcs[0], idcs[2], idcs[1]]))
            uv_idcs.extend([0, 2, 1])
        
        # Construct a DCMesh vertex for each face vertex:
        for i, v in enumerate(positions):

            # Position
            pos = [v.co[0], v.co[2], v.co[1]]

            # UVs
            uvs = [0, 0]
            uv_idx = uv_idx_base + uv_idcs[i]
            if obj.data.uv_layers.active != None:
                uv = obj.data.uv_layers.active.data[uv_idx].uv
                uvs[0] = uv.x
                uvs[1] = 1.0 - uv.y

            # Normal
            n = [v.normal[0], v.normal[2], v.normal[1]]

            # Color
            rgba = [1, 1, 1, 1]
            if len(blender_cols) != 0:
                c = blender_cols[0].data[uv_idx].color
                rgba[0] = int(255 * c[0])
                rgba[1] = int(255 * c[1])
                rgba[2] = int(255 * c[2])
                rgba[3] = int(255 * c[3])

            # Weights
            sorted_weights = []
            if should_export_weights:
                for g in v.groups:
                    print('Adding weight for sorting: {} ({})'.format(obj.vertex_groups[g.group].name, g.weight))
                    sorted_weights.append((g.group, g.weight))
                sorted_weights = sorted(sorted_weights, key=lambda x: x[1], reverse=True)
                empty_weights = 3 - len(sorted_weights)

                dcm_vertex = struct.pack('<fffffBBBBfff', \
                                pos[0], pos[1], pos[2], \
                                uvs[0], uvs[1], \
                                rgba[0], rgba[1], rgba[2], rgba[3], \
                                n[0], n[1], n[2])

                for j in range(0, 3):
                    
                    weight_id_data = array.array('I')
                    weight_value_data = array.array('f')
                    
                    if j < len(sorted_weights) and sorted_weights[j][1] > 0.001:
                        g = sorted_weights[j]
                        g_idx = g[0]
                        group = obj.vertex_groups[g_idx]
                        group_name = group.name.encode('utf-8')
                        
                        bone_id = int(hashlib.md5(group_name).hexdigest()[:8], 16)
                        print('Vertex weight {} for group {}: {}, {}'.format(j, group_name, bone_id, g[1]))
                        weight_id_data.append(bone_id)
                        weight_value_data.append(g[1])
                    else:
                        no_weight_set = 710991
                        print('Vertex weight {} (not set): {}, {}'.format(j, no_weight_set, 0))
                        weight_id_data.append(no_weight_set)
                        weight_value_data.append(0)

                    dcm_vert_weight_arrays.append((weight_id_data, weight_value_data))

            for modifier in obj.modifiers:
                if modifier.type == 'ARMATURE':
                    armature_name = modifier.object.name

            dcm_verts.append(dcm_vertex)

        uv_idx_base += len(idcs)

    # Open output file and write out DCMesh data:

    with open(filepath, 'wb') as f:

        # Define constants for this version of the format:
        version = 1
        header_size = 26
        material_size = 228
        vertex_size = 36
        mesh_header_size = 48
        submesh_header_size = 9

        # Step 1) Fill in the file header and write it out:
        material_count = 1
        mesh_count = 1
        vertex_count = len(dcm_verts)
        armature_count = 0 if len(armature_name) == 0 else 1
        animation_count = 0

        pos_format = 2           # POSITION_FORMAT_3F
        tex0_format = 1          # TEX_COORD_FORMAT_2F
        tex1_format = 0          # TEX_COORD_FORMAT_NONE
        color_format = 1         # COLOR_FORMAT_4UB
        offset_color_format = 0  # COLOR_FORMAT_NONE
        normal_format = 1        # NORMAL_FORMAT_3F
        bone_weights_format = 1  # BONE_WEIGHTS_FORMAT_3US_3F
        
        index_size = 4

        header = struct.pack('<cccBBBBBBBBBBBBB', \
                             b'D', b'C', b'M', \
                             version, \
                             material_count, \
                             mesh_count, \
                             armature_count, \
                             animation_count, \
                             pos_format, \
                             tex0_format, \
                             tex1_format, \
                             color_format, \
                             offset_color_format, \
                             normal_format, \
                             index_size, \
                             bone_weights_format)

        f.write(header)

        # Step 2) Write out all of the materials:
        # NOTE(ross): Only 1 material currently supported
        material = {}

        # DataHeader
        f.write(struct.pack('B', 1)) # flags (DATA_FLAG_EXTERNAL_LINK)
        f.write(struct.pack('B', 1)) # local_id for this material is 1
        f.write(struct.pack('<32s', bytes(get_texture_name(obj), 'utf-8')))

        ambient = [0, 0, 0, 1]
        f.write(struct.pack('<ffff', ambient[0], ambient[1], ambient[2], ambient[3]))

        diffuse = [0, 0, 0, 1]
        f.write(struct.pack('<ffff', diffuse[0], diffuse[1], diffuse[2], diffuse[3]))

        specular = [0, 0, 0, 1]
        f.write(struct.pack('<ffff', specular[0], specular[1], specular[2], specular[3]))

        emission = [0, 0, 0, 1]
        f.write(struct.pack('<ffff', emission[0], emission[1], emission[2], emission[3]))

        shininess = 0
        f.write(struct.pack('<f', shininess))

        material['diffuse_map']  = struct.pack('<32s', bytes(get_texture_name(obj), 'utf-8'))
        material['light_map']    = struct.pack('<32s', bytes('', 'utf-8'))
        material['normal_map']   = struct.pack('<32s', bytes('', 'utf-8'))
        material['specular_map'] = struct.pack('<32s', bytes('', 'utf-8'))

        f.write(material['diffuse_map'])
        f.write(material['light_map'])
        f.write(material['normal_map'])
        f.write(material['specular_map'])

        # Step 3) Fill in and write out the mesh header:
        submesh_count = 1

        # DataHeader
        f.write(struct.pack('B', 0)) # flags (none)
        f.write(struct.pack('B', 1)) # local_id for this mesh is 1
        f.write(struct.pack('<32s', bytes(obj.name, 'utf-8')))

        vertex_count = len(dcm_verts)
        mesh_header = struct.pack('<BL', \
                                  submesh_count, \
                                  vertex_count)
        f.write(mesh_header)

        # Step 4) Write out all the mesh vertices:
        for i, v in enumerate(dcm_verts):
            f.write(v)
            for j in range(0, 3):
                dcm_vert_weight_arrays[i * 3 + j][0].tofile(f)
                dcm_vert_weight_arrays[i * 3 + j][1].tofile(f)

        # Step 5) Write out all of the submeshes:
        # NOTE(ross): Only 1 submesh is currently supported
        material_id = 1
        arrangement = 2 # SUB_MESH_ARRANGEMENT_TRIANGLES
        submesh_type = 2 # SUB_MESH_TYPE_INDEXED
        num_ranges_or_indices = vertex_count

        # DataHeader
        f.write(struct.pack('B', 0)) # flags (none)
        f.write(struct.pack('B', 1)) # local_id for this submesh is 1
        f.write(struct.pack('<32s', bytes(obj.data.name, 'utf-8')))

        submesh_header = struct.pack('<BBBH', \
                                     material_id, \
                                     arrangement, \
                                     submesh_type, \
                                     num_ranges_or_indices)

        f.write(submesh_header)

        # Indices
        for i in range(0, vertex_count):
            f.write(struct.pack('<L', i))

        # Step 6) Write out armatures
        # DataHeader
        f.write(struct.pack('B', 1)) # flags (DATA_FLAG_EXTERNAL_LINK)
        f.write(struct.pack('B', 1)) # local_id for this armature is 1
        f.write(struct.pack('<32s', bytes(armature_name, 'utf-8')))

        # Step 7) Write out animations
        # NOTE(ross): Currently no animations saved inline
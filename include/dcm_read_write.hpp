#pragma once
#include <iostream>

namespace dcm {

inline bool read_file_header(dcm::FileHeader & file_header, std::istream & fin) {

    fin.read((char *) file_header.id, sizeof(file_header.id));
    fin.read((char *) &file_header.version, sizeof(file_header.version));
    fin.read((char *) &file_header.material_count, sizeof(file_header.material_count));
    fin.read((char *) &file_header.mesh_count, sizeof(file_header.mesh_count));
    fin.read((char *) &file_header.armature_count, sizeof(file_header.armature_count));
    fin.read((char *) &file_header.animation_count, sizeof(file_header.animation_count));
    fin.read((char *) &file_header.pos_format, sizeof(file_header.pos_format));
    fin.read((char *) &file_header.tex0_format, sizeof(file_header.tex0_format));
    fin.read((char *) &file_header.tex1_format, sizeof(file_header.tex1_format));
    fin.read((char *) &file_header.color_format, sizeof(file_header.color_format));
    fin.read((char *) &file_header.offset_color_format, sizeof(file_header.offset_color_format));
    fin.read((char *) &file_header.normal_format, sizeof(file_header.normal_format));
    fin.read((char *) &file_header.index_size, sizeof(file_header.index_size));
    fin.read((char *) &file_header.bone_weights_format, sizeof(file_header.bone_weights_format));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_file_header(dcm::FileHeader const & file_header, std::ostream & fout) {

    fout.write((char *) file_header.id, sizeof(file_header.id));
    fout.write((char *) &file_header.version, sizeof(file_header.version));
    fout.write((char *) &file_header.material_count, sizeof(file_header.material_count));
    fout.write((char *) &file_header.mesh_count, sizeof(file_header.mesh_count));
    fout.write((char *) &file_header.armature_count, sizeof(file_header.armature_count));
    fout.write((char *) &file_header.animation_count, sizeof(file_header.animation_count));
    fout.write((char *) &file_header.pos_format, sizeof(file_header.pos_format));
    fout.write((char *) &file_header.tex0_format, sizeof(file_header.tex0_format));
    fout.write((char *) &file_header.tex1_format, sizeof(file_header.tex1_format));
    fout.write((char *) &file_header.color_format, sizeof(file_header.color_format));
    fout.write((char *) &file_header.offset_color_format, sizeof(file_header.offset_color_format));
    fout.write((char *) &file_header.normal_format, sizeof(file_header.normal_format));
    fout.write((char *) &file_header.index_size, sizeof(file_header.index_size));
    fout.write((char *) &file_header.bone_weights_format, sizeof(file_header.bone_weights_format));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_data_header(dcm::DataHeader & data_header, std::istream & fin) {

    fin.read((char *) &data_header.flags, sizeof(data_header.flags));
    fin.read((char *) &data_header.local_id, sizeof(data_header.local_id));
    fin.read((char *) data_header.path, sizeof(data_header.path));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_data_header(dcm::DataHeader const & data_header, std::ostream & fout) {

    fout.write((char *) &data_header.flags, sizeof(data_header.flags));
    fout.write((char *) &data_header.local_id, sizeof(data_header.local_id));
    fout.write((char *) data_header.path, sizeof(data_header.path));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_material(dcm::Material & material, std::istream & fin) {

    read_data_header(material.data_header, fin);
    fin.read((char *) material.ambient, sizeof(material.ambient));
    fin.read((char *) material.diffuse, sizeof(material.diffuse));
    fin.read((char *) material.specular, sizeof(material.specular));
    fin.read((char *) material.emission, sizeof(material.emission));
    fin.read((char *) &material.shininess, sizeof(material.shininess));
    fin.read((char *) material.diffuse_map, sizeof(material.diffuse_map));
    fin.read((char *) material.light_map, sizeof(material.light_map));
    fin.read((char *) material.normal_map, sizeof(material.normal_map));
    fin.read((char *) material.specular_map, sizeof(material.specular_map));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_material(dcm::Material const & material, std::ostream & fout) {

    write_data_header(material.data_header, fout);
    fout.write((char *) material.ambient, sizeof(material.ambient));
    fout.write((char *) material.diffuse, sizeof(material.diffuse));
    fout.write((char *) material.specular, sizeof(material.specular));
    fout.write((char *) material.emission, sizeof(material.emission));
    fout.write((char *) &material.shininess, sizeof(material.shininess));
    fout.write((char *) material.diffuse_map, sizeof(material.diffuse_map));
    fout.write((char *) material.light_map, sizeof(material.light_map));
    fout.write((char *) material.normal_map, sizeof(material.normal_map));
    fout.write((char *) material.specular_map, sizeof(material.specular_map));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_mesh_header(dcm::MeshHeader & mesh_header, std::istream & fin) {

    read_data_header(mesh_header.data_header, fin);
    fin.read((char *) &mesh_header.submesh_count, sizeof(mesh_header.submesh_count));
    fin.read((char *) &mesh_header.vertex_count, sizeof(mesh_header.vertex_count));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_mesh_header(dcm::MeshHeader const & mesh_header, std::ostream & fout) {

    write_data_header(mesh_header.data_header, fout);
    fout.write((char *) &mesh_header.submesh_count, sizeof(mesh_header.submesh_count));
    fout.write((char *) &mesh_header.vertex_count, sizeof(mesh_header.vertex_count));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_2f(float * f, std::istream & fin) {

    fin.read((char *) &f[0], sizeof(f[0]));
    fin.read((char *) &f[1], sizeof(f[1]));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_2f(float const * f, std::ostream & fout) {

    fout.write((char *) &f[0], sizeof(f[0]));
    fout.write((char *) &f[1], sizeof(f[1]));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_3f(float * f, std::istream & fin) {

    fin.read((char *) &f[0], sizeof(f[0]));
    fin.read((char *) &f[1], sizeof(f[1]));
    fin.read((char *) &f[2], sizeof(f[2]));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_3f(float const * f, std::ostream & fout) {

    fout.write((char *) &f[0], sizeof(f[0]));
    fout.write((char *) &f[1], sizeof(f[1]));
    fout.write((char *) &f[2], sizeof(f[2]));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_color_4ub_to_4f_rgba(float * f, std::istream & fin) {

    uint8_t col_cmp = 0;

    fin.read((char *) &col_cmp, sizeof(col_cmp));
    f[0] = col_cmp / 255.f;

    fin.read((char *) &col_cmp, sizeof(col_cmp));
    f[1] = col_cmp / 255.f;

    fin.read((char *) &col_cmp, sizeof(col_cmp));
    f[2] = col_cmp / 255.f;

    fin.read((char *) &col_cmp, sizeof(col_cmp));
    f[3] = col_cmp / 255.f;

    return true; /* TODO: Detect failure to read data */
}

inline bool read_4ub(uint8_t * ub, std::istream & fin) {

    fin.read((char *) &ub[0], sizeof(ub[0]));
    fin.read((char *) &ub[1], sizeof(ub[1]));
    fin.read((char *) &ub[2], sizeof(ub[2]));
    fin.read((char *) &ub[3], sizeof(ub[3]));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_4ub(uint8_t const * ub, std::ostream & fout) {

    fout.write((char *) &ub[0], sizeof(ub[0]));
    fout.write((char *) &ub[1], sizeof(ub[1]));
    fout.write((char *) &ub[2], sizeof(ub[2]));
    fout.write((char *) &ub[3], sizeof(ub[3]));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_vertex_weights_3_ui_f(dcm::VertexWeight_UI_F * weights, std::istream & fin) {

    for (size_t idx = 0; idx < 3; ++idx) {

        dcm::VertexWeight_UI_F & weight = weights[idx];

        fin.read((char *) &weight.id, sizeof(weight.id));
        fin.read((char *) &weight.influence, sizeof(weight.influence));
    }

    return true; /* TODO: Detect failure to read data */
}

inline bool read_submesh_header(dcm::SubMeshHeader & submesh_header, std::istream & fin) {

    read_data_header(submesh_header.data_header, fin);
    fin.read((char *) &submesh_header.material_id, sizeof(submesh_header.material_id));
    fin.read((char *) &submesh_header.arrangement, sizeof(submesh_header.arrangement));
    fin.read((char *) &submesh_header.type, sizeof(submesh_header.type));
    fin.read((char *)&submesh_header.num_ranges_or_indices, sizeof(submesh_header.num_ranges_or_indices));

    return true; /* TODO: Detect failure to read data */
}

inline bool write_submesh_header(dcm::SubMeshHeader const & submesh_header, std::ostream & fout) {

    write_data_header(submesh_header.data_header, fout);
    fout.write((char *) &submesh_header.material_id, sizeof(submesh_header.material_id));
    fout.write((char *) &submesh_header.arrangement, sizeof(submesh_header.arrangement));
    fout.write((char *) &submesh_header.type, sizeof(submesh_header.type));
    fout.write((char *) &submesh_header.num_ranges_or_indices, sizeof(submesh_header.num_ranges_or_indices));

    return true; /* TODO: Detect failure to read data */
}

inline bool read_armature_header(dcm::ArmatureHeader & armature_header, std::istream & fin) {

    read_data_header(armature_header.data_header, fin);
    fin.read((char *) &armature_header.bone_count, sizeof(armature_header.bone_count));

    return true; /* TODO: Detect failure to read data */
}

}